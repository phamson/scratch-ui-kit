package com.android.scratch.model;

public class Profile {
    private int avatar;
    private int food;
    private String profile;
    private String timeAgo;
    private int title;
    private int introduce;
    private String likes;
    private String comments;

    public Profile(int avatar, int food, String profile,
                   String timeAgo, int title, int introduce,
                   String likes, String comments) {
        this.avatar = avatar;
        this.food = food;
        this.profile = profile;
        this.timeAgo = timeAgo;
        this.title = title;
        this.introduce = introduce;
        this.likes = likes;
        this.comments = comments;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getIntroduce() {
        return introduce;
    }

    public void setIntroduce(int introduce) {
        this.introduce = introduce;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
