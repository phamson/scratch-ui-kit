package com.android.scratch.model;

public class MyRecipes {
    private int image;
    private String titlel;
    private String time;
    private String ingredients;

    public MyRecipes(int image, String titlel, String time, String ingredients) {
        this.image = image;
        this.titlel = titlel;
        this.time = time;
        this.ingredients = ingredients;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitlel() {
        return titlel;
    }

    public void setTitlel(String titlel) {
        this.titlel = titlel;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }
}
