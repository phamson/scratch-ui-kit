package com.android.scratch.model;

public class Food {
    private int image;
    private String title;

    public Food(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public Food(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
