package com.android.scratch.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.android.scratch.R;
import com.android.scratch.adapter.FoodAdapter;
import com.android.scratch.adapter.FormulaAdapter;
import com.android.scratch.model.Food;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class ViewRecipeActivity extends AppCompatActivity {
    private LinearLayout mLinearBack;
    private FormulaAdapter mFormulaAdapter;
    private FoodAdapter mFoodAdapter;
    private RecyclerView mRc_menu;
    private ArrayList<Food> mFoods;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private ArrayList<Fragment> mFragments;
    private LinearLayout mLinearCookNow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cook_now);
        initView();
        assignView();
        initData();
        setupViewPage();
    }

    private void assignView() {
        mLinearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mLinearCookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewRecipeActivity.this, CookingModeActivity.class));
            }
        });
    }

    private void initView() {
        mRc_menu = findViewById(R.id.rc_menu);
        mLinearCookNow = findViewById(R.id.linear_cook_now);
        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tab_layout);
        mLinearBack = findViewById(R.id.linear_back);
    }

    private void initData() {
        mFoods = new ArrayList<>();
        mFoods.add(new Food(R.drawable.image_0));
        mFoods.add(new Food(R.drawable.image_1));
        mFoods.add(new Food(R.drawable.image_0));
        mFoods.add(new Food(R.drawable.image_1));
        mFoods.add(new Food(R.drawable.image_0));
        mFoods.add(new Food(R.drawable.image_1));
        mFoods.add(new Food(R.drawable.image_0));
        mFoods.add(new Food(R.drawable.image_1));

        mFormulaAdapter = new FormulaAdapter(this, mFoods);
        mRc_menu.setAdapter(mFormulaAdapter);
        mRc_menu.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
    }

    private void setupViewPage(){
        mFragments = new ArrayList<>();
        mFragments.add(new FormulaFragment());
        mFragments.add(new FormulaFragment());
        mFragments.add(new FormulaFragment());
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(mFragments);

        mViewPager.setAdapter(viewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.getTabAt(0).setText("Ingredients");
        mTabLayout.getTabAt(1).setText("How to Cook");
        mTabLayout.getTabAt(2).setText("Additional Info");

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> fragments;

        private void addFragment(ArrayList<Fragment> fragments){
            this.fragments = fragments;
        }
        public ViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
