package com.android.scratch.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.scratch.R;
import com.android.scratch.adapter.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private View mLayoutTab;
    private TabLayout mTabLayout;
    private ImageView mImgSearch, mImgHome, mImgProfile;
    private ViewPager viewPager;
    private LinearLayout mLinearSettings;
    private ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        initView();
        assignView();
        Log.d("CCC", mTabLayout.getTabCount()+"");
    }

    private void initView(){
        mLinearSettings = findViewById(R.id.linear_settings);
        viewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tab_layout);
        mLayoutTab = findViewById(R.id.layout_tablayout);
        mImgHome = mLayoutTab.findViewById(R.id.icon_home);
        mImgProfile = mLayoutTab.findViewById(R.id.icon_profile);
        mImgSearch = mLayoutTab.findViewById(R.id.icon_search);
        mImgProfile.setImageResource(R.drawable.ic_nav_3_green);
    }

    private void assignView(){
        mImgHome.setOnClickListener(this);
        mImgProfile.setOnClickListener(this);
        mImgSearch.setOnClickListener(this);
        mLinearSettings.setOnClickListener(this);
        setupViewPager();
    }

    private void setupViewPager(){
        if (viewPagerAdapter == null) {
            viewPagerAdapter = new ViewPagerAdapter( getSupportFragmentManager());
            ArrayList<Fragment> fragments = new ArrayList<>();
            fragments.add(new MyProfileFragment());
            fragments.add(new MyProfileFragment());
            fragments.add(new MyProfileFragment());
            viewPagerAdapter.AddFragment(fragments);
            viewPager.setAdapter(viewPagerAdapter);
        }
        mTabLayout.setupWithViewPager(viewPager);
        mTabLayout.getTabAt(0).setText("20 \n Recipes");
        mTabLayout.getTabAt(1).setText("75 \n Saved");
        mTabLayout.getTabAt(2).setText("248 \n Following");
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Bạn có muốn thoát app hay không ?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.linear_settings:
                startActivity(new Intent(MyProfileActivity.this, SettingsActivity.class));
                break;
            case R.id.icon_search:
                mImgSearch.setImageResource(R.drawable.ic_nav_1_green);
                mImgHome.setImageResource(R.drawable.ic_nav_2_black);
                mImgProfile.setImageResource(R.drawable.ic_nav_3_black);

                break;
            case R.id.icon_home:
                mImgSearch.setImageResource(R.drawable.ic_nav_1_black);
                mImgHome.setImageResource(R.drawable.ic_nav_2_green);
                mImgProfile.setImageResource(R.drawable.ic_nav_3_black);
                startActivity(new Intent(MyProfileActivity.this, RecipeFeedActivity.class));
                break;
            case R.id.icon_profile:
                mImgSearch.setImageResource(R.drawable.ic_nav_1_black);
                mImgHome.setImageResource(R.drawable.ic_nav_2_black);
                mImgProfile.setImageResource(R.drawable.ic_nav_3_green);

                break;
        }
    }
}
