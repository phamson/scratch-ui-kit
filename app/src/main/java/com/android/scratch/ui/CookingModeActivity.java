package com.android.scratch.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.scratch.R;

public class CookingModeActivity extends AppCompatActivity {
    LinearLayout linear_back;
    private ImageView mImgFullScreen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cooking_mode);
        mImgFullScreen = findViewById(R.id.img_full_screen);
        linear_back = findViewById(R.id.linear_back);
        linear_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CookingModeActivity.this,ViewRecipeActivity.class));
            }
        });

        mImgFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CookingModeActivity.this, FullScreenActivity.class));
            }
        });
    }
}
