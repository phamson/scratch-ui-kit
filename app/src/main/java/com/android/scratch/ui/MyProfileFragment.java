package com.android.scratch.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.scratch.R;
import com.android.scratch.adapter.FoodAdapter;
import com.android.scratch.model.Food;

import java.util.ArrayList;

public class MyProfileFragment extends Fragment {
    private View view;
    private FoodAdapter mFoodAdapter;
    private ArrayList<Food> mFoods;
    private RecyclerView mRcView;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_kitchen, container,
                false);
        return view;
    }

    private void initData(){
        if (mFoods == null){
            mFoods = new ArrayList<>();
            mFoods.add(new Food(R.drawable.maskgroup_1));
            mFoods.add(new Food(R.drawable.maskgroup_2));
            mFoods.add(new Food(R.drawable.maskgroup_3));
            mFoods.add(new Food(R.drawable.maskgroup_4));
            mFoods.add(new Food(R.drawable.maskgroup_5));
            mFoods.add(new Food(R.drawable.maskgroup_6));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mRcView = view.findViewById(R.id.rc_kitchen);
        initData();
        mFoodAdapter = new FoodAdapter(getContext(), mFoods);
        mRcView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        mRcView.setAdapter(mFoodAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
