package com.android.scratch.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.scratch.R;
import com.android.scratch.adapter.FoodAdapter;
import com.android.scratch.model.Food;

import java.util.ArrayList;

public class FormulaFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private FoodAdapter mFoodAdapter;
    private ArrayList<Food> mFoods;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.formula_fragment,
                container , false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mFoods = new ArrayList<>();
        mFoods.add(new Food(R.drawable.cooking_spray, "cooking spray"));
        mFoods.add(new Food(R.drawable.whole_milk, "1/2 cup whole milk"));
        mFoods.add(new Food(R.drawable.large_eggs, "2 large eggs 1 tablespoon maple syrup"));
        mFoods.add(new Food(R.drawable.teaspoon, "1/2 teaspoon vanilla extract"));

        mFoodAdapter = new FoodAdapter(getContext(), mFoods);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mFoodAdapter);

    }


}
