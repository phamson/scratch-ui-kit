package com.android.scratch.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.android.scratch.R;
import com.android.scratch.adapter.ProfileAdapter;
import com.android.scratch.model.Profile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RecipeFeedActivity extends AppCompatActivity implements View.OnClickListener {
    private View mLayoutToolbar, mLayoutTablayout;
    private RecyclerView mRcProfile;
    private Map<Integer , ArrayList<Profile> > profiles;
    private ProfileAdapter mAdapter;
    private ImageView mImgSearch, mImgHome, mImgProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        initView();
        initData();
        assignView();

    }

    private void initView(){
        mRcProfile = findViewById(R.id.rc_profile);
        mLayoutToolbar = findViewById(R.id.layout_toolbar_main);
        mLayoutTablayout = findViewById(R.id.layout_tablayout);
        mImgHome = mLayoutTablayout.findViewById(R.id.icon_home);
        mImgProfile = mLayoutTablayout.findViewById(R.id.icon_profile);
        mImgSearch = mLayoutTablayout.findViewById(R.id.icon_search);
        mImgHome.setImageResource(R.drawable.ic_nav_2_green);
    }

    private void assignView(){
        mAdapter = new ProfileAdapter(this, profiles);
        mRcProfile.setAdapter(mAdapter);
        mRcProfile.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        mImgHome.setOnClickListener(this);
        mImgProfile.setOnClickListener(this);
        mImgSearch.setOnClickListener(this);
    }

    private void initData(){
        profiles = new HashMap<>();
        ArrayList<Profile> menu_0 = new ArrayList<>();
        menu_0.add(new Profile(
                R.drawable.avata_0, R.drawable.chocolate_cobbler,
                "Profile Name", "2h ago",R.string.title_1, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_0.add(new Profile(
                R.drawable.avata_0, R.drawable.red_wine,
                "Profile Name", "2h ago",R.string.title_0, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_0.add(new Profile(
                R.drawable.avata_0, R.drawable.cooked_coconut,
                "Profile Name", "2h ago",R.string.title_2, R.string.introduce,
                "32 likes", "8 Comments"));

        ArrayList<Profile> menu_1 = new ArrayList<>();
        menu_1.add(new Profile(
                R.drawable.avata_0, R.drawable.chocolate_cobbler,
                "Profile Name", "2h ago",R.string.title_1, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_1.add(new Profile(
                R.drawable.avata_5, R.drawable.whitewine_scroll,
                "Profile Name", "2h ago",R.string.title_3, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_1.add(new Profile(
                R.drawable.avata_0, R.drawable.cooked_coconut,
                "Profile Name", "2h ago",R.string.title_2, R.string.introduce,
                "32 likes", "8 Comments"));

        ArrayList<Profile> menu_2 = new ArrayList<>();
        menu_2.add(new Profile(
                R.drawable.avata_0, R.drawable.chocolate_cobbler,
                "Profile Name", "2h ago",R.string.title_1, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_2.add(new Profile(
                R.drawable.avata_6, R.drawable.vinillapud_scroll,
                "Profile Name", "2h ago",R.string.title_4, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_2.add(new Profile(
                R.drawable.avata_0, R.drawable.cooked_coconut,
                "Profile Name", "2h ago",R.string.title_2, R.string.introduce,
                "32 likes", "8 Comments"));

        ArrayList<Profile> menu_3 = new ArrayList<>();
        menu_3.add(new Profile(
                R.drawable.avata_0, R.drawable.chocolate_cobbler,
                "Profile Name", "2h ago",R.string.title_1, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_3.add(new Profile(
                R.drawable.avata_4, R.drawable.vuredvegatables_scroll,
                "Profile Name", "2h ago",R.string.title_5, R.string.introduce,
                "32 likes", "8 Comments"));
        menu_3.add(new Profile(
                R.drawable.avata_0, R.drawable.cooked_coconut,
                "Profile Name", "2h ago",R.string.title_2, R.string.introduce,
                "32 likes", "8 Comments"));

        profiles.put(0, menu_0);
        profiles.put(1, menu_1);
        profiles.put(2, menu_2);
        profiles.put(3, menu_3);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Bạn có muốn thoát app hay không ?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.icon_search:
                mImgSearch.setImageResource(R.drawable.ic_nav_1_green);
                mImgHome.setImageResource(R.drawable.ic_nav_2_black);
                mImgProfile.setImageResource(R.drawable.ic_nav_3_black);

                break;
            case R.id.icon_home:
                mImgSearch.setImageResource(R.drawable.ic_nav_1_black);
                mImgHome.setImageResource(R.drawable.ic_nav_2_green);
                mImgProfile.setImageResource(R.drawable.ic_nav_3_black);

                break;
            case R.id.icon_profile:
                mImgSearch.setImageResource(R.drawable.ic_nav_1_black);
                mImgHome.setImageResource(R.drawable.ic_nav_2_black);
                mImgProfile.setImageResource(R.drawable.ic_nav_3_green);
                startActivity(new Intent(RecipeFeedActivity.this, MyProfileActivity.class));
                break;
        }
    }
}
