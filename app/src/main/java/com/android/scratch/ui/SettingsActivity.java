package com.android.scratch.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.scratch.R;

public class SettingsActivity extends AppCompatActivity {
    private SwitchCompat mSwitch;
    private TextView mTextSpan;
    private LinearLayout mLinearBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mTextSpan = findViewById(R.id.text_introduce);
        mLinearBack = findViewById(R.id.img_back);
        mLinearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Spannable wordtoSpan = new SpannableString("If disabled, you won’t be able to see recipes saved by other profiles. Leave this enabled to share your collected recipes to others. why this matter?");

        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)),
                wordtoSpan.length() - 16,
                wordtoSpan.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mTextSpan.setText(wordtoSpan);
    }
}
