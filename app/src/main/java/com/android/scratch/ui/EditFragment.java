package com.android.scratch.ui;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.scratch.Constant;
import com.android.scratch.R;
import com.android.scratch.model.Food;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

public class EditFragment extends BottomSheetDialogFragment {
    private ImageView imageView;
    private ArrayList<Food> foods;

    public void initData(){
        if (foods == null){
            foods = new ArrayList<>();
            foods.add(new Food(R.drawable.image_0_5_2));
            foods.add(new Food(R.drawable.image_1_5_2));
            foods.add(new Food(R.drawable.image_2_5_3));

            foods.add(new Food(R.drawable.image_0_5_2));
            foods.add(new Food(R.drawable.image_1_5_2));
            foods.add(new Food(R.drawable.image_2_5_3));

            foods.add(new Food(R.drawable.image_0_5_2));
            foods.add(new Food(R.drawable.image_1_5_2));
            foods.add(new Food(R.drawable.image_2_5_3));
        }

    }
    public static EditFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString("type",type);
        EditFragment fragment = new EditFragment();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getTheme() {
        return R.style.BottomSheetDialogTheme;
    }
    private String type;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // show full bottom sheet dialog
        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                View bottomSheetInternal =
                        d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        type = getArguments().getString("type");
        View view = null;
        switch (type){
            case Constant.TYPE.Additional_Info:
                view = LayoutInflater.from(getContext()).inflate(R.layout.layout_additional_info,
                            container, false);
            break;
            case Constant.TYPE.Edit_Directions:
                view = LayoutInflater.from(getContext()).inflate(R.layout.layout_edit_directions,
                        container, false);
                break;
            case Constant.TYPE.Edit_Ingredients:
                view = LayoutInflater.from(getContext()).inflate(R.layout.layout_edit_ingredients,
                        container, false);
                break;
            case Constant.TYPE.Edit_Gallery:
                view = LayoutInflater.from(getContext()).inflate(R.layout.layout_edit_gallery,
                        container, false);
                break;
        }
        imageView = view.findViewById(R.id.img_close);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (type.equals("Edit_Gallery")){

        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


}
