package com.android.scratch.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.scratch.Constant;
import com.android.scratch.R;
import com.android.scratch.adapter.IngredientsAdapter;
import com.android.scratch.model.Food;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

public class EditRecipeActivity extends AppCompatActivity implements View.OnClickListener{
    private RecyclerView mRcIngredients;
    private TextView mTextTitle;
    private IngredientsAdapter mAdapter;
    private ArrayList<Food> mFoods;
    private ImageView mImageAdditional, mImageIngredients, mImageDirections, mImageGallery;
    public SlidingUpPanelLayout mSlidingUpPanelLayout;
    private FrameLayout mFrameLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);
        initView();
        initData();
        assignView();
    }

    private void initView() {
        mTextTitle = findViewById(R.id.text_title);
        mImageGallery = findViewById(R.id.image_gallery);
        mImageDirections = findViewById(R.id.image_directions);
        mImageIngredients = findViewById(R.id.image_ingredients);
        mImageAdditional = findViewById(R.id.image_additional);
        mRcIngredients = findViewById(R.id.rc_ingredients);
        mFrameLayout = findViewById(R.id.container);
    }

    private void initData(){
        mFoods = new ArrayList<>();
        mFoods.add(new Food(R.drawable.image5_0, "Lemonade"));
        mFoods.add(new Food(R.drawable.image5_1, "coconut"));
        mFoods.add(new Food(R.drawable.image5_2,"peppers"));
        mFoods.add(new Food(R.drawable.image5_3, "egg"));

        mFoods.add(new Food(R.drawable.image5_0, "Lemonade"));
        mFoods.add(new Food(R.drawable.image5_1, "coconut"));
        mFoods.add(new Food(R.drawable.image5_2,"peppers"));
        mFoods.add(new Food(R.drawable.image5_3, "egg"));

        mFoods.add(new Food(R.drawable.image5_0, "Lemonade"));
        mFoods.add(new Food(R.drawable.image5_1, "coconut"));
        mFoods.add(new Food(R.drawable.image5_2,"peppers"));
        mFoods.add(new Food(R.drawable.image5_3, "egg"));
    }

    private void assignView(){
        mAdapter = new IngredientsAdapter(this, mFoods);
        mRcIngredients.setAdapter(mAdapter);
        mRcIngredients.setLayoutManager(new LinearLayoutManager(this , RecyclerView.HORIZONTAL, false));
        String title = "";
        for (int i = 0; i < mFoods.size();i ++){
            if (i < 4){
                if (i == 4){
                    title += mFoods.get(i).getTitle()+ " ";
                }else if (i < 4){
                    title += mFoods.get(i).getTitle()+", ";
                }

            }else {
                title += "+5 more";
                break;
            }
        }
        mTextTitle.setText(title);
        mImageIngredients.setOnClickListener(this);
        mImageGallery.setOnClickListener(this);
        mImageDirections.setOnClickListener(this);
        mImageAdditional.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (mSlidingUpPanelLayout.getPanelState() == PanelState.EXPANDED){
//            mSlidingUpPanelLayout.setPanelState(PanelState.HIDDEN);
//        }else {
//
//        }
    }

    public void initFragment(String type){
        FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
        mTransaction.replace(mFrameLayout.getId(), EditFragment.newInstance(type));
        mTransaction.commit();
    }
    private String FRAGMENT_TAG = "fragment_tag";
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_additional:
                EditFragment fragment = EditFragment.newInstance(Constant.TYPE.Additional_Info);
                fragment.show(getSupportFragmentManager(),FRAGMENT_TAG );

                break;
            case R.id.image_gallery:
                EditFragment gallery = EditFragment.newInstance(Constant.TYPE.Edit_Gallery);
                gallery.show(getSupportFragmentManager(),FRAGMENT_TAG );

                break;
            case R.id.image_ingredients:
                final BottomSheetDialog mDialogIngredients = new BottomSheetDialog(
                        EditRecipeActivity.this, R.style.BottomSheetDialogTheme);
                mDialogIngredients.setContentView(R.layout.layout_edit_ingredients);
                mDialogIngredients.setCancelable(false);

                ImageView close_ingredients = mDialogIngredients.findViewById(R.id.img_close);
                close_ingredients.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialogIngredients.dismiss();
                    }
                });
                mDialogIngredients.show();
                break;
            case R.id.image_directions:

                EditFragment fragment4 = EditFragment.newInstance(Constant.TYPE.Edit_Directions);

                fragment4.show(getSupportFragmentManager(),FRAGMENT_TAG );

                break;
        }
    }


}
