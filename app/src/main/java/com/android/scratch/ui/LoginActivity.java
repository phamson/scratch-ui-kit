package com.android.scratch.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.scratch.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView mTextCreateHere, mTextTitle, mTextLoginHere;
    private LinearLayout mLinearLogin, mLinearCreate;
    private Button mBtnLogin, mBtnCreateAccount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        assignView();
    }

    private void assignView() {
        mBtnLogin.setOnClickListener(this);
        mTextCreateHere.setOnClickListener(this);
        mBtnCreateAccount.setOnClickListener(this);
        mTextLoginHere.setOnClickListener(this);
    }

    private void initView() {
        mTextLoginHere = findViewById(R.id.text_login_here);
        mTextTitle = findViewById(R.id.text_title);
        mTextCreateHere = findViewById(R.id.text_create_here);
        mLinearCreate = findViewById(R.id.linear_create_account);
        mLinearLogin = findViewById(R.id.linear_login);
        mBtnLogin = findViewById(R.id.btn_login);
        mBtnCreateAccount = findViewById(R.id.btn_create_account);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.text_create_here:
                mTextTitle.setText(R.string.start_from_scratch);
                mLinearLogin.setVisibility(View.GONE);
                mLinearCreate.setVisibility(View.VISIBLE);

                break;
            case R.id.btn_login:
                startActivity(new Intent(LoginActivity.this, RecipeFeedActivity.class));
                break;
            case R.id.text_login_here:
                getWindow().setStatusBarColor(Color.TRANSPARENT);
                break;
            case R.id.btn_create_account:
                mTextTitle.setText(R.string.wellcome);
                mLinearCreate.setVisibility(View.GONE);
                mLinearLogin.setVisibility(View.VISIBLE);
                break;
        }
    }
}
