package com.android.scratch.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.scratch.R;
import com.android.scratch.adapter.MyRecipesAdapter;
import com.android.scratch.model.MyRecipes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyRecipeActivity extends AppCompatActivity {
    private List<String> mTitle;
    private List<MyRecipes> mMyRecipes;
    private MyRecipesAdapter mMyRecipesAdapter;
    private ExpandableListView mExpandableMain;
    private LinearLayout mLinearBack;
    private TextView mTextAddNew;
    private HashMap<String, List<MyRecipes>> mData = new HashMap<String, List<MyRecipes>>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_my_recipe);
        initData();
        initView();
        assignView();
    }

    private void assignView() {
        mLinearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyRecipeActivity.this, MyProfileActivity.class));
            }
        });

        mMyRecipesAdapter = new MyRecipesAdapter(this, mTitle, mData);
        mExpandableMain.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                startActivity(new Intent(MyRecipeActivity.this, EditRecipeActivity.class));
                return false;
            }
        });
        mExpandableMain.setAdapter(mMyRecipesAdapter);
        mTextAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyRecipeActivity.this, NewRecipeActivity.class));
            }
        });
    }

    private void initView() {
        mExpandableMain = findViewById(R.id.expandable_main);
        mTextAddNew = findViewById(R.id.text_add_new);
        mLinearBack = findViewById(R.id.linear_back);
    }

    public void initData(){
        mMyRecipes = new ArrayList<>();
        mMyRecipes.add(new MyRecipes(R.drawable.cooked_coconut,"Cooked Coconut Mussels",
                "\u00B1 5 mins", "4 ingredients"));
        mMyRecipes.add(new MyRecipes(R.drawable.banana,"Banana and Mandarin Buns",
                "\u00B1 45 mins", "4 ingredients"));
        mMyRecipes.add(new MyRecipes(R.drawable.fried_salty,"Fried Salty & Sour Snapper",
                "\u00B1 45 mins", "4 ingredients"));
        mMyRecipes.add(new MyRecipes(R.drawable.cooked_coconut,"Tenderized Hazelnut Pheasant",
                "\u00B1 45 mins", "4 ingredients"));

        mData.put("Western ("+ mMyRecipes.size()+")", mMyRecipes);
        mTitle = new ArrayList<>(mData.keySet());
    }


}
