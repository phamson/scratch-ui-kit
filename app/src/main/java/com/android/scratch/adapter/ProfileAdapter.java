package com.android.scratch.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.scratch.R;
import com.android.scratch.model.Profile;

import java.util.ArrayList;
import java.util.Map;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    private Context context;
    private Map<Integer, ArrayList<Profile>> mProfiles;
    public ProfileAdapter(Context context,Map<Integer, ArrayList<Profile>> mProfiles) {
        this.mProfiles = mProfiles;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_profile_main, null);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mProfiles.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ArrayList<Profile> profiles = mProfiles.get(position);
        Adapter adapter = new Adapter(profiles);
        holder.setData(adapter);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private RecyclerView rc_profile;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rc_profile = itemView.findViewById(R.id.item_rc_profile);
        }
        public void setData(final RecyclerView.Adapter adapter){

            final LinearLayoutManager layoutManager = new LinearLayoutManager(context,
                    RecyclerView.HORIZONTAL, false);
            rc_profile.setAdapter(adapter);
            rc_profile.setLayoutManager(layoutManager);
            rc_profile.scrollToPosition(1);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView img_avatar, img_food;
        private TextView textProfile, textTime, textTitle, textComment, textLikes, textIntroduce;
        private LinearLayout linearSave;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            img_avatar = itemView.findViewById(R.id.img_avatar);
            img_food = itemView.findViewById(R.id.img_food);
            textComment = itemView.findViewById(R.id.text_comment);
            textProfile = itemView.findViewById(R.id.text_profile);
            textTime = itemView.findViewById(R.id.text_time);
            textTitle = itemView.findViewById(R.id.text_title);
            textLikes = itemView.findViewById(R.id.text_likes);
            textIntroduce = itemView.findViewById(R.id.text_introduce);
            linearSave = itemView.findViewById(R.id.linearLikes);
        }

        public void setData(Profile profile){
            img_avatar.setImageResource(profile.getAvatar());
            img_food.setImageResource(profile.getFood());
            textComment.setText(profile.getComments());
            textProfile.setText(profile.getProfile());
            textTime.setText(profile.getTimeAgo());
            textTitle.setText(profile.getTitle());
            textLikes.setText(profile.getLikes());
            textIntroduce.setText(profile.getIntroduce());
        }
    }

    public class Adapter extends RecyclerView.Adapter<ItemViewHolder>{
        private ArrayList<Profile> profiles;

        public Adapter(ArrayList<Profile> profiles) {
            this.profiles = profiles;
        }

        @NonNull
        @Override
        public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_profile_holder, parent
                    , false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
            Profile profile = profiles.get(position);
            holder.setData(profile);
            holder.linearSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_save_to);
                    dialog.setCancelable(false);
                    ImageView imageView = dialog.findViewById(R.id.img_close);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return profiles.size();
        }
    }
}

