package com.android.scratch.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.scratch.R;
import com.android.scratch.model.Food;
import com.android.scratch.ui.MyRecipeActivity;
import com.android.scratch.ui.MyProfileActivity;

import java.util.ArrayList;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder> {
    private ArrayList<Food> mFoods;
    private Context context;

    public FoodAdapter(Context context, ArrayList<Food> foods) {
        this.mFoods = foods;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_food_holder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mFoods.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Food food = mFoods.get(position);
        holder.setData(food);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof MyProfileActivity) {
                    context.startActivity(new Intent(context, MyRecipeActivity.class));
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_food);
            textView = itemView.findViewById(R.id.text_title);
        }

        public void setData(Food food){
            imageView.setImageResource(food.getImage());
            if (food.getTitle() != null){
                textView.setText(food.getTitle());
            }
        }

    }

}
