package com.android.scratch.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.scratch.R;
import com.android.scratch.model.Food;
import com.android.scratch.ui.MyRecipeActivity;
import com.android.scratch.ui.MyProfileActivity;

import java.util.ArrayList;

public class FormulaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Food> mFoods;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public FormulaAdapter(Context context, ArrayList<Food> mFoods) {
        this.mFoods = mFoods;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_food_holder, parent, false);
            return new ItemViewHolder(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_load_more, parent, false);
            return new LoadViewHolder(view);
        }
    }

    @Override
    public int getItemCount() {

        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return position < 2? VIEW_TYPE_ITEM: VIEW_TYPE_LOADING;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            Food food = mFoods.get(position);
            ((ItemViewHolder) holder).setData(food);
            ((ItemViewHolder) holder).imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context instanceof MyProfileActivity) {
                        context.startActivity(new Intent(context, MyRecipeActivity.class));
                    }
                }
            });
        }else if (holder instanceof LoadViewHolder){
            ((LoadViewHolder)holder).setSize(mFoods.size());
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_food);
            textView = itemView.findViewById(R.id.text_title);
        }

        public void setData(Food food){
            imageView.setImageResource(food.getImage());
            if (food.getTitle() != null){
                textView.setText(food.getTitle());
            }
        }
    }

    public class LoadViewHolder extends RecyclerView.ViewHolder{
        TextView textSize;
        ImageView imageView;
        public LoadViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_more);
            imageView.setImageResource(R.drawable.image_more);
            textSize = itemView.findViewById(R.id.text_size);
        }
        public void setSize(int size){
            textSize.setText(size+"+");
        }
    }
}
