package com.android.scratch.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Outline;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.scratch.R;
import com.android.scratch.model.MyRecipes;
import com.android.scratch.ui.CookingModeActivity;
import com.android.scratch.ui.MyRecipeActivity;
import com.android.scratch.ui.ViewRecipeActivity;

import java.util.HashMap;
import java.util.List;

public class MyRecipesAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> mTitles;
    private HashMap<String, List<MyRecipes>> mData;

    public MyRecipesAdapter(Context context, List<String> mTitles, HashMap<String,
            List<MyRecipes>> expandableList) {
        this.context = context;
        this.mTitles = mTitles;
        this.mData = expandableList;
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mData.get(mTitles.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mData.get(mTitles.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private boolean isChoose = false;
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = mTitles.get(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_parrent_my_recipes, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.text_title);
        ImageView imageView = convertView.findViewById(R.id.img_arrow);

        if (!isChoose){
            imageView.setImageResource(R.drawable.ic_icon_arrow_down);
            isChoose = true;
        }else {
            imageView.setImageResource(R.drawable.ic_up);
            isChoose = false;
        }
        listTitleTextView.setText(listTitle);
        return convertView;
    }
    private float curveRadius = 20F;
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        MyRecipes myRecipes = (MyRecipes) getChild(groupPosition, childPosition);
        LinearLayout linear_cook_now;
        ImageView image_food;
        TextView text_title, text_time, text_ingredients;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_child_my_recipes,
                    null);
        }
        image_food = convertView.findViewById(R.id.image_food);

        text_title = convertView.findViewById(R.id.text_title);
        text_time = convertView.findViewById(R.id.text_time);
        text_ingredients = convertView.findViewById(R.id.text_ingredients);
        linear_cook_now = convertView.findViewById(R.id.linear_cook_now);

        linear_cook_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ViewRecipeActivity.class));
            }
        });
        image_food.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setRoundRect(0,0,view.getWidth(),(int) (view.getHeight() + curveRadius),
                        curveRadius);
            }
        });
        image_food.setImageResource(myRecipes.getImage());
        image_food.setClipToOutline(true);
        text_title.setText(myRecipes.getTitlel());
        text_time.setText(myRecipes.getTime());
        text_ingredients.setText(myRecipes.getIngredients());
        Log.d("CCC", "getChildView: Enter");
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        Log.d("CCC", "isChildSelectable: "+childPosition);
        return true;
    }
}
