package com.android.scratch;

public class Constant {
    public @interface TYPE{
        String Additional_Info = "Additional_Info";
        String Edit_Directions = "Edit_Directions";
        String Edit_Gallery = "Edit_Gallery";
        String Edit_Ingredients = "Edit_Ingredients";
    }
}
